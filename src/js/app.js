document.addEventListener("DOMContentLoaded", function(){
   const buttonOpen= document.querySelector('.button-js');
   const buttonClose= document.querySelector('.close-js');
   const modal = document.querySelector('div.modal');

   buttonOpen.addEventListener('click', function(){
      modal.style.display='flex';
   });

   buttonClose.addEventListener('click', function(){
    modal.style.display='none';
   });
// menu

const menuBtn = document.querySelector('.menu-button-js');
const header = document.querySelector('header.header');
menuBtn.addEventListener('click', function(){
header.classList.toggle('mobile-menu');

})


})